//
//  PhotoCell.swift
//  PhotoApp
//
//  Created by alessandro cignolini on 02/05/2019.
//  Copyright © 2019 alessandro cignolini. All rights reserved.
//

import UIKit
import SDWebImage

class PhotoCell: UITableViewCell {
    @IBOutlet var usernameLabel: UILabel!
    
    @IBOutlet var dateLabel: UILabel!
    
    @IBOutlet var photoImageView: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    func setPhoto(_ photo:Photo) {
        usernameLabel.text = photo.byUsername
        dateLabel.text = photo.date
        
        if let urlString = photo.url {
            let url = URL(string: urlString)
            
            guard url != nil else {
                //couldn't create url object
                return
            }
            
            photoImageView.sd_setImage(with: url) {(image, error, cacheType, url) in
                self.photoImageView.image = image
            }
        }
       
    }

}
