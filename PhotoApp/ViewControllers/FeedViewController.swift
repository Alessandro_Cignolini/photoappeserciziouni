//
//  FeedViewController.swift
//  PhotoApp
//
//  Created by alessandro cignolini on 24/01/19.
//  Copyright © 2019 alessandro cignolini. All rights reserved.
//

import UIKit
class FeedViewController: UIViewController {
    @IBOutlet var tableView: UITableView!
    var photos = [Photo]()
    override func viewDidLoad() {
        super.viewDidLoad()
        //configure the tableview
        tableView.dataSource = self
        tableView.delegate = self
        addRefreshControl()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        //retrive and display the photos
        PhotoService.getPhotos(completion: { (photos) in
            self.photos = photos
            self.tableView.reloadData()
        })
    }
    
    func addRefreshControl(){
        let refreshControl = UIRefreshControl()
        refreshControl.addTarget(self, action: #selector(refreshInitiated(refreshControl:)), for: .valueChanged)
        tableView.addSubview(refreshControl)
    }
    @objc func refreshInitiated(refreshControl:UIRefreshControl){
        //call the photo service retrive photos
        PhotoService.getPhotos{(photos) in
            self.photos = photos
            self.tableView.reloadData()
            refreshControl.endRefreshing()
        }
    }


}
extension FeedViewController: UITableViewDelegate, UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return photos.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        //get photo cell
        let cell = tableView.dequeueReusableCell(withIdentifier: Constants.Storyboard.feedPhotoTableCellId, for: indexPath) as! PhotoCell
        //get the photo for this row
        let photo = photos[indexPath.row]
        
        //set the details for the cell
        cell.setPhoto(photo)
        return cell
    }
    
    
}
