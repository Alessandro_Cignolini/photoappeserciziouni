//
//  LoginViewController.swift
//  PhotoApp
//
//  Created by alessandro cignolini on 24/01/19.
//  Copyright © 2019 alessandro cignolini. All rights reserved.
//

import UIKit
import FirebaseUI

class LoginViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    @IBAction func loginTapped(_ sender: UIButton) {
        //create firebase auth ui ibject
        let authUI = FUIAuth.defaultAuthUI()
        //check that it isn't nil
        if let authUI = authUI {
            //create a firebase auth pre build UI View Controller
            let authViewController = authUI.authViewController()
            //set the login view controller as the delegate
            authUI.delegate = self
            // present it
            present(authViewController, animated: true, completion: nil)
        }
    }
    
    

}

extension LoginViewController : FUIAuthDelegate{
    func authUI(_ authUI: FUIAuth, didSignInWith authDataResult: AuthDataResult?, error: Error?) {
        //check if an error occured
        guard error == nil else {
            print("an error has happened")
            return
        }
        //Get the user
        let user = authDataResult?.user
        
        //Check if user is nil
        if let user = user {
            //this mean that we have a user, now check if they have a profile
            UserService.getUserProfile(userId: user.uid) {(u) in
                if u == nil{
                    //no profile, go to Profile Controller
                    self.performSegue(withIdentifier: Constants.Segue.profileViewController, sender: self)
                    
                }else{
                    //save the logged in user to local storage
                    LocalStorageService.saveCurrentUser(user: u!)
                    
                    //this user has a profile, go to tab controller
                    let tabBarVC = self.storyboard?.instantiateViewController(withIdentifier: Constants.Storyboard.tabBarController)
                    self.view.window?.rootViewController = tabBarVC
                    self.view.window?.makeKeyAndVisible()
                }
            }
        }
    }
}
