//
//  CameraViewController.swift
//  PhotoApp
//
//  Created by alessandro cignolini on 24/01/19.
//  Copyright © 2019 alessandro cignolini. All rights reserved.
//

import UIKit
import UICircularProgressRing

class CameraViewController: UIViewController {
    @IBOutlet var progressRing: UICircularProgressRing!
    @IBOutlet var doneLabel: UILabel!
    @IBOutlet var doneButton: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
    
        //hide and configure the progress ring
        progressRing.alpha = 0
        progressRing.value = 0
        progressRing.maxValue = 100
        progressRing.innerRingColor = .green
        
        //hide label and button
        doneLabel.alpha = 0
        doneButton.alpha = 0
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    func savePhoto(image:UIImage) {
        PhotoService.savePhoto(image: image){(pct) in
            //update the progress ring
            self.progressRing.alpha = 1
            self.progressRing.startProgress(to: CGFloat(pct), duration: 1)
            
            if pct == 100 {
                self.doneButton.alpha = 1
                self.doneLabel.alpha = 1
            }
        }
    }
    
    @IBAction func doneTapped(_ sender: Any) {
        //go to the photos tab
        let tabBarVC = self.tabBarController as? MainTabBarController
        
        if let tabBarVC = tabBarVC{
            //Call the goToFeed method
            tabBarVC.goToFeed()
        }
    }
    
}
