//
//  SettingsViewController.swift
//  PhotoApp
//
//  Created by alessandro cignolini on 24/01/19.
//  Copyright © 2019 alessandro cignolini. All rights reserved.
//

import UIKit
import FirebaseAuth

class SettingsViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    

    @IBAction func singOutTapped(_ sender: Any) {
        do{
            //sing out using firebase auth methods
            try Auth.auth().signOut()
            //clear local storage
            LocalStorageService.clearCurrentUser()
            
            //change the window to show the login screen
            let loginVC = storyboard?.instantiateViewController(withIdentifier: Constants.Storyboard.loginNavController)
            view.window?.rootViewController = loginVC
            view.window?.makeKeyAndVisible()
        }catch{
            //error sign out
            print("couldn't sing out")
        }
    }
    

}
