//
//  CreateProfileViewController.swift
//  PhotoApp
//
//  Created by alessandro cignolini on 24/01/19.
//  Copyright © 2019 alessandro cignolini. All rights reserved.
//

import UIKit
import FirebaseAuth

class CreateProfileViewController: UIViewController {

    @IBOutlet var usernameTextField: UITextField!
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    @IBAction func confirmTapped(_ sender: UIButton) {
        //check that there's a user logged in becasue we need the uid
        guard Auth.auth().currentUser != nil else {
            //no user logged in
            print("no user logged in")
            return
        }
        //check that the textfield has a valid name
        let username = usernameTextField.text?.trimmingCharacters(in: .whitespacesAndNewlines)
        guard username != nil && username != "" else {
            print("bad user name")
            return
        }
        //call User Service to create the profile
        UserService.createUserProfile(userId: Auth.auth().currentUser!.uid, username: username!){(u) in
            //check if the profile was created
            if u == nil {
                //error occured in profile saving
                return
            }
            else{
                //save user to local storage
                LocalStorageService.saveCurrentUser(user: u!)
                //go to the tab bar controller
                let tabBarVc = self.storyboard?.instantiateViewController(withIdentifier: Constants.Storyboard.tabBarController)
                self.view.window?.rootViewController = tabBarVc
                self.view.window?.makeKeyAndVisible()
            }
        }
        
    }
    
}
