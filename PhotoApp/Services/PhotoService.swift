//
//  PhotoService.swift
//  PhotoApp
//
//  Created by alessandro cignolini on 24/04/2019.
//  Copyright © 2019 alessandro cignolini. All rights reserved.
//

import Foundation
import Firebase

class PhotoService {
    static func getPhotos(completion: @escaping ([Photo]) -> Void) -> Void {
        //Getting a reference to the database
        let dbRef = Database.database().reference()
        
        //Make the db call
        dbRef.child("photos").observeSingleEvent(of: .value) {(snapshot) in
            //declare an array to hold the photos
            var retrievedPhotos = [Photo]()
            
            //get the list of snapshots
            let snapshots = snapshot.children.allObjects as? [DataSnapshot]
            if let snapshots = snapshots {
                //Loop through each snapshot and parse out the photos
                for snap in snapshots{
                    //Try to create a photo from snapshot
                    let p = Photo(snapshot: snap)
                    //if successful, then add it our array
                    if p != nil{
                        retrievedPhotos.insert(p!, at: 0)
                    }
                }
            }
            //after parsing the snapshots, call the completion closure
            completion(retrievedPhotos)
        }
    }
    static func savePhoto(image:UIImage, progressUpdate: @escaping (Double) -> Void) {
        //get data rapresentation of the image
        let photoData = image.jpegData(compressionQuality:0.1)
        guard photoData != nil else{
            print("couldn't turn the image into data")
            return
        }
        //get storage reference
        let userid = Auth.auth().currentUser!.uid
        let filename = UUID().uuidString
        
        let ref = Storage.storage().reference().child("image/\(userid)/\(filename).jpg")
        
        //upload the photo
        let uploadTask = ref.putData(photoData!, metadata: nil){(metadata, error) in
            
            if error != nil{
                //An error during upload occurred
            }else{
                //Upload was successful, now create a database entry
                self.createPhotoDatabaseEntry(ref: ref)
            }
        }
        uploadTask.observe(.progress) { (snapshot) in
        let percentage:Double = Double(snapshot.progress!.completedUnitCount) / Double(snapshot.progress!.totalUnitCount) * 100
            progressUpdate(percentage)
        }
    }
    private static func createPhotoDatabaseEntry(ref: StorageReference){
        //Get download url for the photo
        ref.downloadURL{(url, error) in
            if error != nil {
                //Couldn't retrieve the url
                return
            }else{
                //get the meta data for the db entry
                //User
                let user = LocalStorageService.loadCurrentUser()
                
                guard user != nil else {
                    return
                }
                //date
                let dateFormatter = DateFormatter()
                dateFormatter.dateStyle = .full
                let dateString = dateFormatter.string(from: Date())
                
                //create photo data
                let photoData = ["byId": user!.userId!,
                                 "byUsername": user!.username!, "date": dateString,
                                 "url": url!.absoluteString]
                //Write a database entry
                let dbRef = Database.database().reference().child("photos").childByAutoId()
                dbRef.setValue(photoData, withCompletionBlock: {(error, dbRef)in
                    if error != nil{
                        //there was an error in writing the database entry
                        return
                    }
                    else{
                        //Database entry for the photo was written
                    }
                })
            }
        }
    }
}


