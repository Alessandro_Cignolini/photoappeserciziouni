//
//  UserService.swift
//  PhotoApp
//
//  Created by alessandro cignolini on 25/01/19.
//  Copyright © 2019 alessandro cignolini. All rights reserved.
//

import Foundation
import FirebaseDatabase

class UserService {
    static func getUserProfile(userId:String, completion: @escaping(PhotoUser?) -> Void)-> Void{
        //get database reference
        let ref = Database.database().reference()
        //try to retrive the profile for the passed in userId
        ref.child("users").child(userId).observeSingleEvent(of: .value) { (snapshot) in
            //check the returned snapshot value to see if there's a profile
            if let userProfileData = snapshot.value as? [String:Any]{
                //this mean there is a profile
                //Create a photo user with the profile details
                var u = PhotoUser()
                u.userId = snapshot.key
                u.username = userProfileData["username"] as? String
                
                //pass it into the completion clourse
                completion(u)
            }else{
                //this mean there wasn't a profile
                //return nil
                completion(nil)
            }
        }
    }
    
    static func createUserProfile(userId:String, username:String, completion: @escaping (PhotoUser?)-> Void)->Void{
        //create a directory for the user profile
        let userProfileData = ["username":username]
        
        //Get a database reference
        let ref = Database.database().reference()
        
        //create the profile for the user id
        ref.child("users").child(userId).setValue(userProfileData){(error, ref) in
            if(error != nil){
                //there was an error
                completion(nil)
            }else{
                //create the user and pass it back
                let u = PhotoUser(username:username, userId: userId)
                completion(u)
            }
        }
    }
}
