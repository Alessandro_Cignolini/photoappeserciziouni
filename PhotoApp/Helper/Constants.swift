//
//  Constants.swift
//  PhotoApp
//
//  Created by alessandro cignolini on 25/01/19.
//  Copyright © 2019 alessandro cignolini. All rights reserved.
//

import Foundation
struct Constants {
    struct Storyboard {
        static let tabBarController = "MainTabBarController"
        static let loginNavController = "LoginNavController"
        static let feedPhotoTableCellId = "PhotoCell"
    }
    struct Segue {
        static let profileViewController = "goToCreateProfile"
    }
    struct LocalStorage {
        static let storedUsername = "storedUsername"
        static let storedUserId = "storedUserId"
    }

}

